﻿using Dynamics365.Abstractions;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Threading.Tasks;

namespace Dynamics365.Sdk.Connection
{
    public class XrmConnection
    {
        public IOrganizationService Service => service;
        public XrmGuid CurrentUser { get; protected set; }

        public Task<bool> ConnectToOffice365Async(string crmUrl, string username, string password)
        {
            return Task.Factory.StartNew(() =>
            {
                crmServiceClient = new CrmServiceClient($"AuthType=Office365;Url={crmUrl};Username={username};Password={password}");
                if (crmServiceClient is not null)
                {
                    service = crmServiceClient;
                    CurrentUser = ((WhoAmIResponse)Service.Execute(new WhoAmIRequest())).UserId;
                    return CurrentUser != Guid.Empty;
                }

                return false;
            });
        }
    
        public XrmGuid Caller
        {
            get => crmServiceClient.CallerId;
            set => crmServiceClient.CallerId = value;
        }

        protected IOrganizationService service = default!;
        protected CrmServiceClient crmServiceClient = default!;
    }
}
