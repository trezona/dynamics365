﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;

namespace Dynamics365.Common.Platforms.Net
{
    public class XrmConnect
    {
        public IOrganizationService? Service { get; }

        public XrmConnect(string url, string username, string password, string connectMode = "Office365")
        {
            try
            {
                Console.WriteLine("Setting up Dynamics 365 connection");
                Service = new CrmServiceClient($"AuthType={connectMode};Url={url};Username={username};Password={password}");

                if (Service != null)
                {
                    var userId = ((WhoAmIResponse)Service.Execute(new WhoAmIRequest())).UserId;
                    if (userId != Guid.Empty)
                    {
                        Console.WriteLine($"    -> Connection Successful!");
                    }
                }
                else
                {
                    Console.WriteLine(" -> Connection failed...");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(" -> Error - " + ex.ToString());
            }
        }

    }
}
