﻿using Dynamics365.Abstractions.Interfaces;

namespace Dynamics365.Abstractions
{
    public abstract class XrmTable : IXrmEntity
    {

        // GetAttribute
        public abstract T GetAttribute<T>(string attributeName);
        public abstract object GetAttribute(string attributeName);

        // TryGetAttribute
        public abstract bool TryGetAttribute(string attributeName, out object value);
        public abstract bool TryGetAttribute<T>(string attributeName, out T value);

        // SetAttribute
        public abstract void SetAttribute<T>(string attributeName, T attributeValue);
        public abstract object this[string attributeName] { get; set; }
    }
}
