﻿using System;

namespace Dynamics365.Abstractions
{
    public struct XrmGuid
    {
        public Guid Value { get; set; }

        public XrmGuid(Guid guid) => Value = guid;
        public XrmGuid(string guid) => Value = new Guid(guid);

        // Implicit Operators
        static public implicit operator XrmGuid(Guid guid) => new(guid);
        static public implicit operator XrmGuid(string guid) => new(guid);
        static public implicit operator Guid(XrmGuid xrmGuid) => xrmGuid.Value;

        // Overrides
        public override string ToString() => Value.ToString();
        public override int GetHashCode() => Value.GetHashCode();
        public override bool Equals(object obj)
        {
            return obj switch
            {
                Guid guid => Value.Equals(guid),
                string stringGuid => Value.Equals(new Guid(stringGuid)),
                _ => base.Equals(obj)
            };
        }
    }
}
