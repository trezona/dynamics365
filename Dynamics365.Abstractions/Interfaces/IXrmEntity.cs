﻿namespace Dynamics365.Abstractions.Interfaces
{
    public interface IXrmEntity
    {
        public string EntityName { get; }
        public XrmGuid EntityId { get; }

        public bool TryGetAttribute(string attributeName, out object value);
        public bool TryGetAttribute<T>(string attributeName, out T value);

        public object GetAttribute(string attributeName);
        public T GetAttribute<T>(string attributeName);

        public void SetAttribute<T>(string attributeName, T attributeValue);
        public object this[string attributeName] { get; set; }

    }
}
