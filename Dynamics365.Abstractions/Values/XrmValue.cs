﻿using Dynamics365.Abstractions.Interfaces;
using Dynamics365.Abstractions.Values.Interfaces;

namespace Dynamics365.Abstractions.Values
{
    public class XrmValue : IXrmValue
    {
        protected XrmValue(IXrmEntity xrmEntity, string attributeName, object attributeValue) : this(xrmEntity, attributeName)
        {
            AttributeValue = attributeValue;
        }

        protected XrmValue(IXrmEntity xrmEntity, string attributeName)
        {
            this.xrmEntity = xrmEntity;
            AttributeName = attributeName.ToLower();
        }

        // Interface
        public virtual string AttributeName { get; }
        public virtual object AttributeValue
        {
            get => xrmEntity.GetAttribute(AttributeName);
            set => xrmEntity.SetAttribute(AttributeName, value);
        }

        // Overrides
        public override string ToString() => string.Format("{0}[{1}]", AttributeName, AttributeValue);
        public override int GetHashCode() => AttributeName.GetHashCode();
        public override bool Equals(object obj)
        {
            return obj switch
            {
                IXrmValue xrmValue => AttributeName.Equals(xrmValue.AttributeName),
                string attributeName => AttributeName.Equals(attributeName, System.StringComparison.OrdinalIgnoreCase),
                _ => base.Equals(obj)
            };
        }

        protected readonly IXrmEntity xrmEntity;
    }
}
