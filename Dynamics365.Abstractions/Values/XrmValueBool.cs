﻿using Dynamics365.Abstractions.Interfaces;

namespace Dynamics365.Abstractions.Values
{
    public class XrmValueBool : XrmValue<bool>
    {
        protected XrmValueBool(IXrmEntity xrmEntity, string attributeName) : base(xrmEntity, attributeName) { }
        protected XrmValueBool(IXrmEntity xrmEntity, string attributeName, bool attributeValue) : base(xrmEntity, attributeName, attributeValue) { }
    }
}
