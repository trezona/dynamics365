﻿using Dynamics365.Abstractions.Interfaces;

namespace Dynamics365.Abstractions.Values
{
    public class XrmValueString : XrmValue<string>
    {
        protected XrmValueString(IXrmEntity xrmEntity, string attributeName) : base(xrmEntity, attributeName) { }
        protected XrmValueString(IXrmEntity xrmEntity, string attributeName, string attributeValue) : base(xrmEntity, attributeName, attributeValue) { }
    }
}
